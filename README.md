# Indexer

This document assumes you have an understanding of the purpose and function of database composite indexes and the general best practices in identifying indexes for basic SELECT queries. The chapter on indexing in the [High Performance MySQL](https://www.highperfmysql.com/) book is a great place to start

## Architecture

- Indexer works by parsing pt-query-digest json output from MySQL slow query logs, identifing potential index columns
- It then ranks those columns bases on a cost (number of rows scanned in slow queries) and cardinality (number of different values in the column, a major factor in query efficiency)
- Starting with a list of existing indexes it then adds new composite indexes starting with the highest value columns.
- It also removes redundant indexes that are covered by new composite indexes.
- Finally it stores the indexes it has determined to add and has an optional facility that will add the same indexes when new tables with matching column names are created.

### Slow query log processing workflow

```mermaid
 %%{init: { 'theme':'default' } }%%
graph TD
    pt-query-digest -->|json query digest| drush[drush indexer:process]
    drush --> json(parse json)
    json --> queries(parse queries)
    queries --> |tables, columns, query cost| tables[/for each table\]
    tables --> existing(fetch existing indexes as tree)
    existing --> columns[/for each column\]
    columns --> length{table length}
    subgraph index value
    length -->|short| count(count distinct) --> value
    length -->|long| count-random(count distinct from random rows) --> value
    value[value = cardinality x cost] --> columns
    end
    subgraph manage indexes
    value --> merge(merge columns into index tree, high value first)
    merge --> walk[/walk final index tree\]
    walk -->|leaf & index doesn't exist| add(add index)
    walk -->|branch & index already exists| remove(remove redundant index)
    end
    add --> tables
    remove --> tables
```

### Index tree

This data structure is used to merge newly identified indexes into the existing ones and identify which indexes to add and remove to result in the best composite indexes. Indexes nearer the root of the tree have been identified as higher value and are therefore first in the list of composite indexes.

```mermaid
 %%{init: { 'theme':'default' } }%%
graph LR
    subgraph 1 [foo_bar_baz]
    1foo{{foo}} --> 1bar{{bar}} --> 1baz{{baz}}
    end
    subgraph 2 [foo_qux_quux]
    2foo{{foo}} --> 2qux{{qux}} --> 2quux{{quux}}
    end
    subgraph 3 [foo_qux_quuz]
    3foo{{foo}} --> 3qux{{qux}} --> 3quuz{{quuz}}
    end
    subgraph tree
    tfoo{{foo}} --> tbar{{bar}}
    tfoo{{foo}} --> tqux{{qux}}
    tbar{{bar}} --> tbaz{{baz}}
    tqux{{qux}} --> tquux{{quux}}
    tqux{{qux}} --> tquuz{{quuz}}
    end
    tbaz -.- 1foo
    tquux -.- 2foo
    tquuz -.- 3foo
```
