<?php

namespace Drupal\indexer;

use Drush\Commands\DrushCommands;
use PHPSQLParser\PHPSQLParser;
use Drupal\Core\Database\Connection;

/**
 * Drush commands.
 */
class Drush extends DrushCommands {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'indexer.settings';

  /**
   * The SQL parser.
   *
   * @var \PHPSQLParser\PHPSQLParser
   */
  protected $parser;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs this factory object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The Connection object.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
    $this->parser = new PHPSQLParser();
  }

  /**
   * Process a pt-query-digest json output slow query log file.
   *
   * @param string $file
   *   The filename of pt-query-digest json output.
   * @param array $options
   *   Command options.
   *
   * @command indexer:process
   *
   * @option max-composite-index
   *   The maximum number of columns that should be included in composite
   *   indexes.
   * @option min-sample-size
   *   The minimum sample size to accept for cardinality estimates.
   *   If this many rows (or more) are not returned, the command will emit an
   *   error message and stop. If the table has fewer rows than this then all
   *   rows will be used to determine cardinality.
   * @option max-sample-size
   *   The maximum sample size count to target for cardinality estimates.
   *   Note: if primary keys have many gaps (due to deletions) then the number
   *   of rows in the estimate will be lower and you may want to increase this.
   * @option dry-run
   *   Print ALTER queries instead of running them.
   *
   * @bootstrap database
   */
  public function process(string $file, array $options = [
    'max-composite-index' => '3',
    'min-sample-size' => '1000',
    'max-sample-size' => '20000',
    'store-state' => TRUE,
    'dry-run' => FALSE,
  ]) {
    $config = \Drupal::config(static::SETTINGS);
    $tablePrefix = $config->get('table_prefix');
    $prefixLen = $config->get('string_prefix_len');
    try {
      $content = file_get_contents($file);
      $json = json_decode($content);
      $this->logger->info("Extracting columns and costs from pt-query-digest json output");
      $data = $this->extractData($json);
    }
    catch (\Exception $e) {
      $this->logger->error("We were not able to load and parse the file {$file} - check this is readable and valid pt-query-digest json output");
      $this->logger->debug($e->getMessage());
    }
    $this->logger->info("Extracted slow queries across {count} tables from json", ['count' => count($data)]);
    foreach ($data as $table => $queries) {
      if ($tablePrefix != '' && strpos($table, $tablePrefix) === FALSE) {
        $this->logger->info("{table}: Doesn't match configured prefix, skipping", ['table' => $table]);
        continue;
      }

      $this->logger->info("{table}: Generating a list of existing indexes", ['table' => $table]);
      list($existingIndexList, $existingIndexTree, $primaryKey) = $this->getExistingIndexes($table);

      $this->logger->info("{table}: Calculating potential value of an index for each column in table", ['table' => $table]);
      $values = $this->getIndexValues($table, $queries, $primaryKey, $options['min-sample-size'], $options['max-sample-size']);

      $this->logger->info("{table}: Generating a tree of new indexes", ['table' => $table]);
      $indexes = $this->getIndexes($queries, $values, $existingIndexTree, $options['max-composite-index']);

      $this->logger->info("{table}: Adding and removing indexes", ['table' => $table]);
      $this->addRemoveIndexes($indexes, [], $table, $existingIndexList, $prefixLen, $options['dry-run']);
    }
  }

  /**
   * Extract a table oriented array from the parsed slow query log.
   *
   * @param object $json
   *   Parsed slow query log JSON object.
   *
   * @return array
   *   Array of tables, together with the column list and cost for each query.
   */
  private function extractData(object $json): array {
    $data = [];
    $this->logger->info("Extracting data from {count} slow query classes", ['count' => count($json->classes)]);
    foreach ($json->classes as $class) {
      $checksum = $class->checksum;
      $query = $this->parser->parse($class->example->query);
      $query = $this->stripCount($query);
      // If we can't identify the table or any columns we skip this query.
      $table = $this->getTable($query);
      if (empty($table)) {
        $this->logger->info("Table for query {checksum} doesn't exist, skipping", ['checksum' => $checksum]);
        continue;
      }
      $columns = $this->getAllColumns($query);
      if (empty($columns)) {
        $this->logger->info("Couldn't find columns for query {checksum}, skipping", ['checksum' => $checksum]);
        continue;
      }
      $data[$table][$checksum]['columns'] = $columns;
      $data[$table][$checksum]['cost'] = $class->metrics->Rows_examined->sum;
    }
    return $data;
  }

  /**
   * Build a list and tree of existing indexes for a specifed table.
   *
   * The list is used to be able to quickly check if an existing index is
   * present, whilst the tree is used as a starting point to ensure we don't
   * add any new indexes that are covered by existing ones.
   *
   * @param string $table
   *   Table to build indexes for.
   *
   * @return array
   *   First element is a list of indexes, the second is a tree of existing
   *   indexes and the third is the name of the primary key column (if it is an
   *   integer).
   */
  private function getExistingIndexes(string $table): array {
    $table = $this->connection->escapeTable($table);
    $existingIndexList = [];
    $existingIndexTree = [];
    $indexes = [];
    $primaryKey = FALSE;
    $rows = $this->connection
      ->query('SHOW INDEX FROM ' . $table)
      ->fetchAll();
    foreach ($rows as $row) {
      $indexes[$row->Key_name][$row->Seq_in_index] = $row->Column_name;
    }
    // Record if we have an integer primary key.
    if (!empty($indexes['PRIMARY'][1])) {
      $info = $this->connection->query("SHOW COLUMNS FROM " . $table . " WHERE FIELD = :primary", [":primary" => $indexes['PRIMARY'][1]])->fetchAssoc();
      if (substr($info['Type'], 0, 3) == 'int') {
        $primaryKey = $indexes['PRIMARY'][1];
      }
    }
    unset($indexes['PRIMARY']);
    foreach ($indexes as $name => $index) {
      // Ensure indexes are sorted by their sequence.
      ksort($index);
      $existingIndexList[implode(':', $index)][] = $name;
      // Reverse the array since we build the tree from the inside out.
      $rindex = array_reverse($index);
      // We use an array as the leaf of the tree so that things merge cleanly.
      $nested = [];
      foreach ($rindex as $column) {
        $nested = [$column => $nested];
      }
      $existingIndexTree = array_merge_recursive($existingIndexTree, $nested);
    }
    return [$existingIndexList, $existingIndexTree, $primaryKey];
  }

  /**
   * Get the performance value of each column as a potential index participant.
   *
   * This takes into account (a) the reported cost of the slow queries (in terms
   * of rows examined) and the (b) an estimate of the cardinality of the column
   * (as a ratio). The the value is the cost multiplied by the cardinality -
   * higher numbers are considered the highest value in terms of index
   * effectiveness.
   *
   * @param string $table
   *   The name of the table being evaluated.
   * @param array $queries
   *   A list of queries and costs for this table.
   * @param string $primaryKey
   *   Integer primary key, if available. If not provided the cardinality
   *   estimate will be skipped.
   * @param int $minSampleSize
   *   The minimum percentage of samples to target for the cardinality estimate,
   *   if less than this are recieved an error will be produced.
   * @param int $maxSampleSize
   *   The maximum number of samples to target for the cardinality estimate.
   *
   * @return array
   *   An array of columns => value.
   */
  private function getIndexValues(string $table, array $queries, string $primaryKey, int $minSampleSize, int $maxSampleSize): array {
    $values = [];
    foreach ($queries as $checksum => $info) {
      foreach ($info['columns'] as $column) {
        if (!isset($values[$column])) {
          $values[$column] = 0;
        }
        $values[$column] += $info['cost'];
      }
    }

    // If we have a primary key and multiple columns to consider we incorporate
    // a cardinality estimate.
    if ($primaryKey && count($values) > 1) {
      $cardinalityEstimate = $this->getCardinalityEstimate($primaryKey, $table, array_keys($values), $minSampleSize, $maxSampleSize);
      foreach ($values as $column => $cost) {
        $values[$column] = $cost * $cardinalityEstimate[$column];
      }
    }
    arsort($values);
    return $values;
  }

  /**
   * Estimate the cardinality of columns in a table.
   *
   * Because we are potentially working with a large unindexed table, we can't
   * simply count all distinct values or just take a random sample using ORDER
   * BY RAND() as these both require full table scans/sorts which can be very
   * slow. Instead, we only estimate this if we have an integer primary key
   * available and select a random set of rows from the primary key column (up
   * to the maximum ID). This is fast, but if the primary key column has gaps
   * will not find exactly $sampleSize rows - it is fast enough though that we
   * can remediate this by simply selecting a large enough $sampleSize that we
   * get sufficient rows for a decent estimate.
   *
   * @param string $primaryKey
   *   An integer primary key.
   * @param string $table
   *   The table to check.
   * @param array $columns
   *   The columns to check.
   * @param int $minSampleSize
   *   The minimum percentage of samples to target for the cardinality estimate,
   *   if less than this are recieved an error will be produced.
   * @param int $maxSampleSize
   *   The maximum number of samples to target for the cardinality estimate.
   *
   * @return array
   *   An array of columns => cardinatity, where the cardinality is
   *   expressed as a ratio of number of distinct values divided by the number
   *   of rows in the actual sample.
   */
  private function getCardinalityEstimate(string $primaryKey, string $table, array $columns, int $minSampleSize, int $maxSampleSize): array {
    $database = $this->connection;

    // Get row count estimate. This query is based on information_schema
    // queries from Drupal core Schema.php.
    $info = $this->connection->getConnectionOptions();
    $condition = $this->connection->condition('AND');
    $condition->condition('table_schema', $info['database']);
    $condition->condition('table_name', $table);
    $condition->compile($this->connection, $this->connection->schema());
    $rowCount = $this->connection->query("SELECT TABLE_ROWS FROM information_schema.tables WHERE " . (string) $condition, $condition->arguments())->fetchField();

    $primaryKey = $database->escapeField($primaryKey);
  
    // If the number of rows is less than minSampleSize we use all rows
    // instead of an estimate - this is simpler and avoids issues where we
    // end up with too few samples.
    if ($rowCount < $minSampleSize) {
      $query = $database->select($table, 'l');
      foreach ($columns as $column) {
        $query->addExpression('COUNT(DISTINCT ' . $database->escapeField($column) . ')', $column);
      }
      $results = $query->execute()->fetchAssoc();
    }
    else {
      // Determing the start ID (min) and the size (max - min) of the random
      // range.
      $minMax = $database->select($table, 'i');
      $minMax->addExpression('MIN(' . $primaryKey . ')', 'min');
      $minMax->addExpression('MAX(' . $primaryKey . ')', 'max');
      $minMaxResults = $minMax->execute()->fetchAssoc();
      $min = $minMaxResults['min'];
      $max = $minMaxResults['max'];
      $size = $max - $min;

      // Generate a list of integers between the minimum and maximum ID.
      $randSubquery = $database->select($table, 'r');
      $randSubquery->addExpression('CEIL(' . $min . ' + (RAND() * ' . $size . '))', 'id');
      $randSubquery->range(0, $maxSampleSize);

      // Calculate a ratio of distinct values verses the number of values
      // returned. We use a ratio since the number of values returned may vary if
      // the ID column has gaps.
      $query = $database->select($table, 'l');
      foreach ($columns as $column) {
        $query->addExpression('COUNT(DISTINCT ' . $database->escapeField($column) . ')/COUNT(l.' . $primaryKey . ')', $column);
      }
      $query->addExpression('COUNT(l.' . $primaryKey . ')', 'row_count');
      $query->innerJoin($randSubquery, 'r', 'l.' . $primaryKey . ' = r.id');
      $results = $query->execute()->fetchAssoc();

      // Throw an error if it looks like we got too few rows.
      if ($results['row_count'] < $minSampleSize) {
        throw new \Exception(dt("We only got !row_count rows for estimating the cardinality of table !table," .
          "which is less than the specified !minSampleSize minimum number of rows. " .
          "You may need to increase --max-sample-size to increase the number of rows selected.",
          [
            '!row_count' => $results['row_count'],
            '!table' => $table,
            '!minSampleSize' => $minSampleSize,
          ]
        ));
      }
    }

    return $results;
  }

  /**
   * Builds a tree of indexes adding new columns into the existing indexes.
   *
   * The key attribute of this function is that: (a) where a query references
   * multiple columns we generate a composite index starting with the most
   * valuable column and (b) we deduplicate indexes, including indexes that are
   * contained within other indexes.
   *
   * @param array $queries
   *   A list of slow queries we want to generate indexes for.
   * @param array $values
   *   A list of values of each column.
   * @param array $existingIndexTree
   *   A tree of existing indexes to be used as a starting point.
   * @param int $maxCompositeIndex
   *   The maximum number of fields to incorporate into composite indexes.
   *
   * @return array
   *   A tree of indexes, with an empty array at the leaf of each branch.
   */
  private function getIndexes(array $queries, array $values, array $existingIndexTree, int $maxCompositeIndex): array {
    $indexes = $existingIndexTree;

    foreach ($queries as $checksum => $info) {
      // Get the list of columns in the query sorted in order of value.
      $columns = array_intersect(array_keys($values), $info['columns']);
      // Constrain the length of composite indexes.
      $columns = array_slice($columns, 0, $maxCompositeIndex);
      // Reverse the order since we build the array from the inside out.
      $columns = array_reverse($columns);
      if (count($columns) > 0) {
        // We use an array as the leaf of the tree so that things merge cleanly.
        $nested = [];
        foreach ($columns as $column) {
          $nested = [$column => $nested];
        }

        // This merges the arrays so that index sequences that are contained
        // within other sequences are preserved.
        $indexes = array_merge_recursive($indexes, $nested);
      }
    }
    return $indexes;
  }

  /**
   * Recurse into the tree and add/remove indexes as needed.
   *
   * @param array $indexes
   *   Tree of indexes (or a sub-tree for a composite index)
   * @param array $chain
   *   Chain of parent indexes for a composite index.
   * @param string $table
   *   Table to add indexes to.
   * @param array $existingIndexList
   *   List of existing indexes.
   * @param string $prefixLen
   *   Index prefix length.
   * @param bool $dryRun
   *   If true, apply indexes - otherwise just print the SQL that would be run.
   */
  private function addRemoveIndexes(array $indexes, array $chain, string $table, array &$existingIndexList, string $prefixLen, bool $dryRun) {
    if (empty($indexes)) {
      // Reached a leaf - if it is not on the existingIndexList then add the
      // index.
      $identifier = implode(':', $chain);
      if (empty($existingIndexList[$identifier])) {
        $this->addIndex($table, $chain, $prefixLen, $dryRun);
      }
      else {
        $this->logger->info("{table}: Index for columns {identifier} already exists",
          ['table' => $table, 'identifier' => $identifier]);
      }
      // Remove any existing indexes with a smaller set of columns that are
      // redundant.
      $redundant = array_slice($chain, 0, -1);
      foreach ($redundant as $i => $column) {
        $identifier = implode(':', array_slice($chain, 0, $i + 1));
        if (!empty($existingIndexList[$identifier])) {
          foreach ($existingIndexList[$identifier] as $indexName) {
            $this->removeIndex($table, $indexName, $dryRun);
          }
          unset($existingIndexList[$identifier]);
        }
      }
    }
    else {
      foreach ($indexes as $column => $compositeIndexes) {
        $compositeChain = $chain;
        $compositeChain[] = $column;
        $this->addRemoveIndexes($compositeIndexes, $compositeChain, $table, $existingIndexList, $prefixLen, $dryRun);
      }
    }
  }

  /**
   * Adds an index to a specified table.
   *
   * @param string $table
   *   The table to add the index to.
   * @param array $columns
   *   An ordered list of columns to create the index on.
   * @param string $prefixLen
   *   The maximum prefix length to be applied to tables that support prefixes.
   * @param bool $dryRun
   *   If true, apply indexes - otherwise just print the SQL that would be run.
   */
  private function addIndex(string $table, array $columns, string $prefixLen, bool $dryRun) {
    $table = $this->connection->escapeTable($table);
    // No existing index - add it now.
    $indexName = $this->generateIndexName($columns);
    $info = $this->connection->query("SHOW COLUMNS FROM " . $table)->fetchAllAssoc('Field');
    $queryColumns = [];
    foreach ($columns as $column) {
      // This captures a base type in the first capture group and an optional
      // length in the second (nested to remove the parens from the capture).
      $regexp = '|^([a-z]+)(?:\(([0-9]+)\))?|';
      $matches = [];
      if (empty($info[$column]) || !preg_match($regexp, $info[$column]->Type, $matches)) {
        $this->logger->error("{table}: Column {column} does not exist or has an unknown type, skipping this column in the index",
          ['table' => $table, 'column' => $column]);
        continue;
      }
      // If we have a column type that supports a prefix then we use the
      // supplied prefix length - or the column length if it is shorter.
      $stringTypes = [
        'tinytext', 'mediumtext', 'text', 'longtext',
        'tinyblob', 'mediumblob', 'blob', 'longblob',
        'char', 'varchar', 'binary', 'varbinary',
      ];
      if (in_array($matches[1], $stringTypes)) {
        $length = $prefixLen;
        if (!empty($matches[2]) && is_numeric($matches[2]) && $matches[2] < $length) {
          $length = $matches[2];
        }
        $queryColumns[] = '`' . $this->connection->escapeField($column) . '`(' . $length . ')';
      }
      else {
        $queryColumns[] = '`' . $this->connection->escapeField($column) . '`';
      }
    }
    if (empty($queryColumns)) {
      $this->logger->error("{table}: Couldn't find any valid columns to index, skipping index",
        ['table' => $table]);
      return;
    }
    $query = 'ALTER TABLE ' . $table . ' ADD INDEX `' . $indexName . '` (' . implode(', ', $queryColumns) . ')';
    $this->logger->notice("{table}: Adding index {indexName}",
      ['table' => $table, 'indexName' => $indexName]);
    if ($dryRun == TRUE) {
      $this->io()->writeln($query);
    }
    else {
      try {
        // Create the index.
        $this->connection->query($query);
        // Record the index pattern in state.
        $state = \Drupal::state()->get('indexer_column_name_state', []);
        $state[$indexName] = $columns;
        \Drupal::state()->set('indexer_column_name_state', $state);
      }
      catch (\Exception $e) {
        $this->logger->error("{table}: Adding index {indexName} failed",
          ['table' => $table, 'indexName' => $indexName]);
        $this->logger->debug($e->getMessage());
      }
    }
  }

  /**
   * Removes a specified index from a table.
   *
   * @param string $table
   *   The table to remove the index from.
   * @param string $indexName
   *   The index name to remove.
   * @param bool $dryRun
   *   If true, apply indexes - otherwise just print the SQL that would be run.
   */
  private function removeIndex(string $table, string $indexName, bool $dryRun) {
    $table = $this->connection->escapeTable($table);
    $query = 'ALTER TABLE ' . $table . ' DROP INDEX `' . $indexName . '`';
    $this->logger->notice("{table}: Dropping redundant index {indexName}",
      ['table' => $table, 'indexName' => $indexName]);
    if ($dryRun == TRUE) {
      $this->io()->writeln($query);
    }
    else {
      try {
        // Drop the index.
        $this->connection->query($query);
        // Remove the index pattern from state (if it exists).
        $state = \Drupal::state()->get('indexer_column_name_state', []);
        if (isset($state[$indexName])) {
          unset($state[$indexName]);
        }
        \Drupal::state()->set('indexer_column_name_state', $state);
      }
      catch (\Exception $e) {
        $this->logger->error("{table}: Dropping redundant {indexName} failed",
          ['table' => $table, 'indexName' => $indexName]);
        $this->logger->debug($e->getMessage());
      }
    }
  }

  /**
   * Generate an index name for a given set of columns.
   *
   * Shorten longest column names until the index name fits a 64 character max
   * identifier length. We also remove underscores in column names to make it
   * clearer what the participating columns are.
   *
   * @param array $columns
   *   List of columns to include in the name.
   *
   * @return string
   *   Index name.
   */
  private function generateIndexName(array $columns): string {
    // Generate an index name.
    // Shorten columns until we have a name that fits MySQL identifier limits.
    $name = '';
    while ($name == '' || strlen($name) > 64) {
      if (strlen($name) > 64) {
        // This gets the key of the current longest column name.
        $mapping = array_combine(array_keys($columns), array_map('strlen', $columns));
        $longest = end(array_keys($mapping, max($mapping)));
        // Remove the last character.
        $columns[$longest] = substr($columns[$longest], 0, -1);
      }
      // Generate or regenerate a name.
      $name = [];
      foreach ($columns as $column) {
        // We remove underscores to make it easier to identify index columns and
        // ensure uniqueness.
        $name[] = str_replace("_", "", $column);
      }
      $name = implode("_", $name);
    }
    return $name;
  }

  /**
   * Strip simple COUNT queries and optimize the subquery instead.
   *
   * @param array $query
   *   Parsed query array to check.
   *
   * @return array
   *   If $query was a simple COUNT query, the subquery being counted.
   */
  private function stripCount(array $query): array {
    if (
      !empty($query['SELECT'])
      && count($query['SELECT']) == 1
      && !empty($query['FROM'])
      && count($query['FROM']) == 1
      && empty($query['WHERE'])
      && !empty($query['SELECT'][0]['expr_type'])
      && !empty($query['FROM'][0]['expr_type'])
      && !empty($query['FROM'][0]['sub_tree']['SELECT'][0]['base_expr'])
      && $query['SELECT'][0]['expr_type'] == 'aggregate_function'
      && $query['SELECT'][0]['base_expr'] == 'COUNT'
      && $query['FROM'][0]['expr_type'] == 'subquery'
      && $query['FROM'][0]['sub_tree']['SELECT'][0]['base_expr'] == 1
    ) {
      return $query['FROM'][0]['sub_tree'];
    }
    return $query;
  }

  /**
   * Returns the table name if it exists and we can support it, otherwise FALSE.
   *
   * @param array $query
   *   Parsed query array to check.
   *
   * @return string
   *   Table name if it exists and we can support it, empty string if not.
   */
  private function getTable(array $query): string {
    $unsupported = [
      'INSERT', 'UPDATE', 'DELETE', 'REPLACE', 'RENAME', 'SHOW', 'SET', 'DROP',
      'CREATE INDEX', 'CREATE TABLE', 'EXPLAIN', 'DESCRIBE',
    ];
    // If it's not a simple select, we can't identify the table or the query
    // involves more than one table then we don't support it.
    if (!empty(array_intersect(array_keys($query), $unsupported))
      || empty($query['FROM'][0]['expr_type'])
      || $query['FROM'][0]['expr_type'] != 'table'
      || count($query['FROM']) != 1) {
      return FALSE;
    }
    $table = $query['FROM'][0]['table'];
    if ($this->connection->schema()->tableExists($table)) {
      return $table;
    }
    return '';
  }

  /**
   * Build a list of direct column aliases.
   *
   * @param array $query
   *   Parsed query array to extract aliases from.
   *
   * @return array
   *   Array of alias => column.
   */
  private function getAliases(array $query): array {
    $aliases = [];
    if (!empty($query['SELECT'])) {
      foreach ($query['SELECT'] as $item) {
        if (!empty($item['alias']['no_quotes']['parts']) && !empty($item['no_quotes']['parts'])) {
          $aliases[$item['alias']['no_quotes']['parts'][0]] = end($item['no_quotes']['parts']);
        }
      }
    }
    return $aliases;
  }

  /**
   * Extracts a list of columns involved in the WHERE or ORDER parts of a query.
   *
   * @param array $query
   *   Parsed query array to extract columns from.
   *
   * @return array
   *   Deduplicated list of columns involved in the query.
   */
  private function getAllColumns(array $query): array {
    // Find columns from WHERE and ORDER clauses if present.
    $columns = [];
    if (!empty($query['WHERE'])) {
      $columns = array_merge($columns, $this->findColumns($query['WHERE']));
    }
    if (!empty($query['ORDER'])) {
      $columns = array_merge($columns, $this->findColumns($query['ORDER']));
    }
    // Replace aliases with column names.
    $aliases = $this->getAliases($query);
    foreach ($columns as $i => $column) {
      if (isset($aliases[$column])) {
        $columns[$i] = $aliases[$column];
      }
    }
    // Remove duplicates and invalid columns.
    $columns = array_unique($columns);
    // ESCAPE is picked up as a column name by the parser, but isn't an actual
    // column.
    $invalid_columns = ['ESCAPE'];
    $columns = array_diff($columns, $invalid_columns);
    return $columns;
  }

  /**
   * Recursively find all columns/aliases involved in a query.
   *
   * @param array $input
   *   Query or query fragment to check.
   *
   * @return array
   *   Array of columns/aliases found.
   */
  private function findColumns(array $input): array {
    $columns = [];
    if (!empty($input['expr_type']) && ($input['expr_type'] == 'colref' || $input['expr_type'] == 'alias')) {
      return [end($input['no_quotes']['parts'])];
    }
    elseif (is_array($input)) {
      foreach ($input as $item) {
        if (is_array($item)) {
          $columns = array_merge($columns, $this->findColumns($item));
        }
      }
    }
    return $columns;
  }

}
