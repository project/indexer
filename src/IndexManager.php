<?php

namespace Drupal\indexer;

use Drupal\migrate\Plugin\migrate\process\Get;

/**
 * Index manager service that modules can use to alter schemas prior to .
 */
class IndexManager {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'indexer.settings';

  /**
   * Update a schema array to include indexes where columns match.
   *
   * @param string $tableName
   *   Name of table being created.
   * @param array $schema
   *   Schema array to check for indexes to add.
   *
   * @return array
   *   Schema array with indexes added.
   */
  public function modifySchema(string $tableName, array $schema) {
    $config = \Drupal::config(static::SETTINGS);

    // Return early if not enabled.
    if (!$config->get('column_name.apply')) {
      return $schema;
    }

    // Return early if a prefix is configured and doesn't match.
    $tablePrefix = $config->get('table_prefix');
    if ($tablePrefix != '' && strpos($tableName, $config->get('table_prefix')) === FALSE) {
      return $schema;
    }

    $allIndexes = \Drupal::state()->get('indexer_column_name_state', []);
    $allIndexes = array_merge($allIndexes, $config->get('column_name.config'));

    // Determine which indexes apply to this table schema based on it's columns,
    // starting from any provided indexes.
    $validIndexes = $schema['indexes'] ?? [];
    $validIndexes += array_filter($allIndexes, function ($columns) use ($schema) {
      return count(array_intersect($columns, array_keys($schema['fields']))) === count($columns);
    });
    // Return early if we have no valid indexes.
    if (empty($validIndexes)) {
      return $schema;
    }

    // Remove any redundant indexes by constructing a tree and pulling out the
    // index names corresponding with the leaves.
    $indexList = [];
    $indexTree = [];
    foreach ($validIndexes as $name => $columns) {
      // If there are duplicates the last name will be selected.
      $indexList[implode(':', $columns)] = $name;
      $rcolumns = array_reverse($columns);
      $nested = [];
      foreach ($rcolumns as $column) {
        $nested = [$column => $nested];
      }
      $indexTree = array_merge_recursive($indexTree, $nested);
    }
    $indexNames = $this->getIndexes($indexTree, $indexList);

    // Build final index list, incorporating string prefix lengths.
    $indexes = [];
    // If we have a column type that supports a prefix then we use the
    // supplied prefix length - or the column length if it is shorter.
    $prefixLen = $config->get('string_prefix_len');
    $stringTypes = [
      'tinytext', 'mediumtext', 'text', 'longtext',
      'tinyblob', 'mediumblob', 'blob', 'longblob',
      'char', 'varchar', 'binary', 'varbinary',
    ];
    foreach ($indexNames as $name) {
      $columns = $validIndexes[$name];
      foreach ($columns as $column) {
        $length = FALSE;
        $type = FALSE;
        // Use a mysql_type if set, use that - otherwise the generic type.
        if (!empty($schema['fields'][$column]['mysql_type'])) {
          $type = $schema['fields'][$column]['mysql_type'];
        }
        elseif (!empty($schema['fields'][$column]['type'])) {
          $type = $schema['fields'][$column]['type'];
        }
        if ($type && in_array($type, $stringTypes)) {
          $length = $prefixLen;
          if (!empty($schema['fields'][$column]['length']) && $length > $schema['fields'][$column]['length']) {
            $length = $schema['fields'][$column]['length'];
          }
        }
        if ($length == FALSE) {
          $indexes[$name][] = $column;
        }
        else {
          $indexes[$name][] = [$column, $length];
        }
      }
    }
    $schema['indexes'] = $indexes;

    return $schema;
  }

  /**
   * Helper function to flatten a tree of indexes.
   *
   * @param array $indexes
   *   Tree (or sub-tree, if called recursively) to flatten.
   * @param array $indexList
   *   List containing names of indexes to add.
   * @param array $chain
   *   Path to root of the tree, when called recursively.
   *
   * @return array
   *   Flattened array of indexes.
   */
  private function getIndexes(array $indexes, array $indexList, array $chain = []) {
    $return = [];
    if (empty($indexes)) {
      // Reached a leaf - add the index.
      $identifier = implode(':', $chain);
      return [$indexList[$identifier]];
    }
    else {
      foreach ($indexes as $column => $compositeIndexes) {
        $compositeChain = $chain;
        $compositeChain[] = $column;
        $return = array_merge($return, $this->getIndexes($compositeIndexes, $indexList, $compositeChain));
      }
    }
    return $return;
  }

}
