<?php

namespace Drupal\indexer\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure indexer settings for this site.
 */
class IndexerSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'indexer.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'indexer_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['table_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Table prefix'),
      '#description' => $this->t('Only process tables with this prefix. This applies to both the Drush command and the column name based index provisioning'),
      '#default_value' => $config->get('table_prefix'),
    ];

    $form['string_prefix_len'] = [
      '#type' => 'textfield',
      '#title' => $this->t('String prefix length'),
      '#description' => $this->t('Generates string indexes as prefix indexes with this maximum length. This applies to both the Drush command and the column name based index provisioning'),
      '#default_value' => $config->get('string_prefix_len'),
    ];

    $form['column_name'] = [
      '#type' => 'fieldset',
      '#title' => t('Column name based index patterns'),
    ];

    $form['column_name']['apply'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Apply column name based indexes to new tables'),
      '#description' => $this->t('If enabled indexes will be added to newly created tables that match the table prefix and contain one or more of the column name patterns below'),
      '#default_value' => $config->get('column_name.apply'),
    ];

    $form['column_name']['help'] = [
      '#type' => 'item',
      '#title' => t('Usage'),
      '#description' => $this->t('These column name patterns from Drupal config and stage will be merged and applied to new tables if enabled above. The config patterns take precedence if the index name also in the state patterns. The expected format is one pattern per line with an index name, equals sign followed by a colon ordered list of columns to index (if present) - i.e.: <pre>indexname=columna:columnb:columnc</pre>'),
    ];

    $form['column_name']['config'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Patterns in configuration'),
      '#default_value' => $this->serializeFormat($config->get('column_name.config')),
    ];

    $form['column_name']['state'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Patterns in state'),
      '#description' => $this->t('The Drush command will merge new index patterns into the state when run.'),
      '#default_value' => $this->serializeFormat(\Drupal::state()->get('indexer_column_name_state', [])),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $form_state->setValue('config', $this->parseFormat($form_state, 'config'));
    $form_state->setValue('state', $this->parseFormat($form_state, 'state'));

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('table_prefix', $form_state->getValue('table_prefix'))
      ->set('string_prefix_len', $form_state->getValue('string_prefix_len'))
      ->set('column_name.apply', $form_state->getValue('apply'))
      ->set('column_name.config', $form_state->getValue('config'))
      ->save();
    \Drupal::state()->set('indexer_column_name_state', $form_state->getValue('state'));

    parent::submitForm($form, $form_state);
  }

  /**
   * Convert an array to the simplified text format.
   *
   * @param array $value
   *   Array value to convert.
   *
   * @return string
   *   Serialized value.
   */
  private function serializeFormat(array $value) {
    $lines = [];
    foreach ($value as $name => $fields) {
      $lines[] = $name . '=' . implode(':', $fields);
    }
    return implode("\n", $lines);
  }

  /**
   * Validate and convert a simplified text format to an array for storage.
   *
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form state containing text.
   * @param string $field
   *   Field to parse and associate error with (if needed).
   *
   * @return array
   *   Array of index name with a value array of columns in order.
   */
  private function parseFormat(FormStateInterface $form_state, string $field) {
    $lines = explode("\n", $form_state->getValue($field));
    $parsed = [];
    foreach ($lines as $line) {
      $line = trim($line);
      if (!empty($line)) {
        // We do a single validation of the format and allowed characters which
        // simplifies parsing.
        if (!preg_match('/([A-Za-z0-9_]+)=([A-Za-z0-9_]+:?)+/', $line)) {
          $form_state->setErrorByName($field, $this->t('Line does not match format or contains invalid characters: @line', ['@line' => $line]));
        }
        $parts = explode("=", $line);
        $name = $parts[0];
        $fields = explode(':', $parts[1]);
        $parsed[$name] = $fields;
      }
    }
    return $parsed;
  }

}
