<?php

namespace Drupal\Tests\indexer\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\indexer\IndexManager;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\State\StateInterface;

/**
 * Index manager service unit tests.
 *
 * @group indexer
 */
class IndexManagerTest extends UnitTestCase {

  /**
   * Set up test environment.
   */
  public function setUp() {
    parent::setUp();

    $config_map = [
      'indexer.settings' => [
        'string_prefix_len' => 50,
        'column_name' => [
          'apply' => TRUE,
          'config' => [
            'year' => [
              'year',
            ],
            'year_price_unused' => [
              'year',
              'price',
            ],
            'year_price' => [
              'year',
              'price',
            ],
            'year_milage' => [
              'year',
              'mileage_thousands',
            ],
            'price_year' => [
              'price',
              'year',
            ],
          ],
        ],
      ],
    ];

    // Get service stubs.
    $config_factory = $this->getConfigFactoryStub($config_map);
    $state = $this->getMockBuilder(StateInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $state->expects($this->any())
      ->method('get')
      ->willReturn([]);

    $container = new ContainerBuilder();
    // Add service stubs to container.
    $container->set('config.factory', $config_factory);
    $container->set('state', $state);

    \Drupal::setContainer($container);
  }

  /**
   * General index test.
   */
  public function testIndexes() {
    $indexManager = new IndexManager();
    $schema = [
      'fields' => [
        'record_number' => [
          'type' => 'serial',
          'unsigned' => 1,
          'not null' => 1,
        ],
        'year' => [
          'type' => 'varchar',
          'length' => 4,
        ],
        'mileage_thousands' => [
          'type' => 'text',
        ],
        'price' => [
          'type' => 'int',
        ],
        'primary key' => [
          0 => 'record_number',
        ],
      ],
    ];
    $schema = $indexManager->modifySchema('test', $schema);
    $expected = [
      'year_price' => [
        0 => [
          0 => 'year',
          1 => 4,
        ],
        1 => 'price',
      ],
      'year_milage' => [
        0 => [
          0 => 'year',
          1 => 4,
        ],
        1 => [
          0 => 'mileage_thousands',
          1 => 50,
        ],
      ],
      'price_year' => [
        0 => 'price',
        1 => [
          0 => 'year',
          1 => 4,
        ],
      ],
    ];
    $this->assertArrayEquals($expected, $schema['indexes']);
  }

}
